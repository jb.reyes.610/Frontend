# Frontend Test App

1. Run `git clone https://gitlab.com/jb.reyes.610/Frontend.git` then `cd Frontend` to navigate to the folder.
2. Run `npm install` to install dependencies and build the project.
3. Run `npm run serve:ssr` to run the server.
4. Navigate to: `http://localhost:4000` to run the app.

## NOTE: Make sure npm/node and git (cli) are installed and your are connected to the internet