import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ErrorPageComponent, Features } from './shared';

export const FEATURES: Features = {
  graphicalEditor: {
    name: 'Graphical Editor', path: 'graphical-editor', preload: true, title: 'Graphical Editor | Frontend'
  },
  locationMapper: {
    name: 'Location Mapper', path: 'location-mapper', preload: true, title: 'Graphical Editor | Frontend'
  }
};

const routes: Routes = [
  {
    path: FEATURES.locationMapper.path,
    loadChildren: './+location-mapper/location-mapper.module#LocationMapperModule',
    data: FEATURES.locationMapper
  },
  {
    path: FEATURES.graphicalEditor.path,
    loadChildren: './+graphical-editor/graphical-editor.module#GraphicalEditorModule',
    data: FEATURES.graphicalEditor
  },
  { path: '', redirectTo: FEATURES.graphicalEditor.path, pathMatch: 'full' },
  { path: '**', component: ErrorPageComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
