import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { environment } from '../../../environments';
import { AuthModule } from './auth';
import { RouterModule } from './router';
import { reducers, metaReducers, effects } from './stores.reducer';

@NgModule({
  imports: [
    StoreModule.forRoot(reducers, { metaReducers }),
    EffectsModule.forRoot(effects),
    environment && !environment.production ? StoreDevtoolsModule.instrument() : [],
    AuthModule,
    RouterModule
  ]
})
export class StoresModule {
  constructor(@Optional() @SkipSelf() parentModule?: StoresModule) {
    if (parentModule) {
      throw new Error(
        'StoresModule is already loaded. Import it in the AppModule or CoreModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: StoresModule
    };
  }
}
