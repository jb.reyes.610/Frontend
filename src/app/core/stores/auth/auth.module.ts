import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { AuthEffects } from './auth.effects';
import { AuthStore } from './auth-store.service';
import { authFeatureName } from './auth.actions';
import { authReducer } from './auth.reducer';

@NgModule({
  imports: [
    StoreModule.forFeature(authFeatureName, authReducer),
    EffectsModule.forFeature([AuthEffects]),
  ],
  providers: [
    AuthStore
  ]
})
export class AuthModule { }
