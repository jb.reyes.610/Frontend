import { Action } from '@ngrx/store';

import { Credentials, Error, type } from '../../../shared';

import { AuthData } from './auth.model';

export const authFeatureName = 'auth';

export const AuthActionTypes = {
  LOGIN: type(`[${authFeatureName}] Login`),
  LOGIN_FAILURE: type(`[${authFeatureName}] Login Failure`),
  LOGIN_SUCCESS: type(`[${authFeatureName}] Login Success`),
  LOGOUT: type(`[${authFeatureName}] Logout`),
  LOGOUT_FAILURE: type(`[${authFeatureName}] Logout Failure`),
  LOGOUT_SUCCESS: type(`[${authFeatureName}] Logout Success`),
};

export class AuthLoginAction implements Action {
  readonly type = AuthActionTypes.LOGIN;
  constructor(public payload: Credentials) {}
}

export class AuthLoginFailureAction implements Action {
  readonly type = AuthActionTypes.LOGIN_FAILURE;
  constructor(public payload: Error) {}
}

export class AuthLoginSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGIN_SUCCESS;
  constructor(public payload: AuthData) {}
}

export class AuthLogoutAction implements Action {
  readonly type = AuthActionTypes.LOGOUT;
  constructor(public payload: AuthData) {}
}

export class AuthLogoutFailureAction implements Action {
  readonly type = AuthActionTypes.LOGOUT_FAILURE;
  constructor(public payload: Error) {}
}

export class AuthLogoutSuccessAction implements Action {
  readonly type = AuthActionTypes.LOGOUT_SUCCESS;
  constructor(public payload?: any) {}
}

export type AuthActions = AuthLoginAction | AuthLoginFailureAction | AuthLoginSuccessAction
  | AuthLogoutAction | AuthLogoutFailureAction | AuthLogoutSuccessAction;
