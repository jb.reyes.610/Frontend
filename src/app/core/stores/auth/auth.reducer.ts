import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Error } from '../../../shared';
import { AuthActions, AuthActionTypes, authFeatureName } from './auth.actions';
import { AuthData } from './auth.model';

export interface AuthState {
  authenticated: boolean;
  loading: boolean;
  data?: AuthData;
  error?: Error;
}

export const authInitialState: AuthState = {
  authenticated: false,
  loading: false
};

export function authReducer (state: AuthState = authInitialState, action: AuthActions): AuthState {
  switch (action.type) {
    case AuthActionTypes.LOGIN:
      return {
        ...state,
        loading: true
      };
    case AuthActionTypes.LOGIN_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case AuthActionTypes.LOGIN_SUCCESS:
      return {
        ...state,
        authenticated: true,
        loading: false,
        data: action.payload
      };
    default:
      return state;
  }
}

export const authFeature = createFeatureSelector<AuthState>(authFeatureName);

export const authSelectors = {
  state: authFeature,
  authenticated: createSelector(authFeature, (state: AuthState) => state.authenticated),
  loading: createSelector(authFeature, (state: AuthState) => state.loading),
  data: createSelector(authFeature, (state: AuthState) => state.data),
  error: createSelector(authFeature, (state: AuthState) => state.error)
};
