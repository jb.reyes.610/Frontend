import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { catchError, map, mergeMap, delay } from 'rxjs/operators';

import { Response } from '../../../shared';
import { HttpService, LoggerService } from '../../services';
import { AuthActionTypes, AuthLoginSuccessAction, AuthLoginFailureAction } from './auth.actions';

@Injectable()
export class AuthEffects {
  @Effect()
  login$: Observable<Action> = this.actions$.pipe(
    ofType(AuthActionTypes.LOGIN),
    delay(2000), // Test loading delay
    mergeMap(action =>
      this.http.mock('/assets/mocks/login-success.mock.json').pipe( // Success mock api
      // this.http.mock('/assets/mocks/login-failure.mock.json').pipe( // Failure mock api
        map((response: Response) => {
          if (response && response.status) {
            this.logger.info(response.message);
            return new AuthLoginSuccessAction(response.data);
          }
          return new AuthLoginFailureAction(response.error);
        }),
        catchError(() => of(new AuthLoginFailureAction({ code: 'ERR001', message: 'Sample error only' })))
      )
    )
  );

  constructor(
    private actions$: Actions,
    private http: HttpService,
    private logger: LoggerService) {}
}
