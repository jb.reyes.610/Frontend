export interface AuthData {
  username: string;
  token: string;
}
