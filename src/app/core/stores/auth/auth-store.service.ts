import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { Credentials, Error } from '../../../shared';
import { AuthData } from './auth.model';
import { AuthLoginAction } from './auth.actions';
import { authSelectors, AuthState } from './auth.reducer';

@Injectable({
  providedIn: 'root'
})
export class AuthStore {
  state$: Observable<AuthState> = this.store$.pipe(select(authSelectors.state));

  authenticated$: Observable<boolean> = this.store$.pipe(select(authSelectors.authenticated));
  loading$: Observable<boolean> = this.store$.pipe(select(authSelectors.loading));
  data$: Observable<AuthData> = this.store$.pipe(select(authSelectors.data));
  error$: Observable<Error> = this.store$.pipe(select(authSelectors.error));

  constructor(private store$: Store<AuthState>) { }

  login(credentials: Credentials): Observable<AuthState> {
    this.store$.dispatch(new AuthLoginAction(credentials));
    return this.state$;
  }
}
