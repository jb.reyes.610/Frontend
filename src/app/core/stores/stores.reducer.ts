import { ActionReducerMap, MetaReducer, ActionReducer } from '@ngrx/store';
import { routerReducer } from '@ngrx/router-store';

import { environment } from '../../../environments';

import { AuthActionTypes, authReducer } from './auth';

export const effects = [];
export const reducers: ActionReducerMap<any> = {
  auth: authReducer,
  router: routerReducer
};

export function debugReducer (reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    const nextState = reducer(state, action);
    if (environment && !environment.production) {
      console.group(action.type);
      console.log(`%c previous state`, `color: #9E9E9E; font-weight: bold`, state);
      console.log(`%c action`, `color: #03A9F4; font-weight: bold`, action);
      console.log(`%c next state`, `color: #4CAF50; font-weight: bold`, nextState);
      console.groupEnd();
    }
    return nextState;
  };
}

export function clearStatesReducer (reducer: ActionReducer<any>): ActionReducer<any> {
  return (state, action) => {
    if (action.type === AuthActionTypes.LOGOUT_SUCCESS) {
      console.log(`%c Clearing states...`, `color: #FA113D; font-weight: bold`);
      state = undefined;
    }
    const nextState = reducer(state, action);
    return nextState;
  };
}

export const metaReducers: MetaReducer<any>[] = !environment.production ? [debugReducer, clearStatesReducer] : [clearStatesReducer];
