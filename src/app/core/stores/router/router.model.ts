import { NavigationExtras, Params } from '@angular/router';

export interface RouterData {
  path: any[];
  query?: object;
  extras?: NavigationExtras;
}
