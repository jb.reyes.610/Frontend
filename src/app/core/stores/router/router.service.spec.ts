import { TestBed, inject } from '@angular/core/testing';

import { RouterStore } from './router-store.service';

describe('RouterService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [RouterStore]
    });
  });

  it('should be created', inject([RouterStore], (service: RouterStore) => {
    expect(service).toBeTruthy();
  }));
});
