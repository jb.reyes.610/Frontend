import { Params, RouterStateSnapshot, Data } from '@angular/router';

import { RouterReducerState, RouterStateSerializer } from '@ngrx/router-store';
import { createFeatureSelector, createSelector } from '@ngrx/store';

import { routerFeatureName } from './router.actions';

export interface RouterState {
  url: string;
  params: Params;
  queryParams: Params;
  data: Data;
}

export class CustomSerializer implements RouterStateSerializer<RouterState> {
  serialize(routerState: RouterStateSnapshot): RouterState {
    let route = routerState.root;

    while (route.firstChild) {
      route = route.firstChild;
    }

    const {
      url,
      root: { queryParams },
    } = routerState;
    const { params } = route;
    const { data } = route;

    return { url, params, queryParams, data };
  }
}

export const routerFeature = createFeatureSelector<RouterReducerState<RouterState>>(routerFeatureName);

export const routerSelectors = {
  state: routerFeature,
  params: createSelector(routerFeature, (reducerState: RouterReducerState<RouterState>) =>
    reducerState && reducerState.state.params),
  queryParams: createSelector(routerFeature, (reducerState: RouterReducerState<RouterState>) =>
    reducerState && reducerState.state.queryParams),
  url: createSelector(routerFeature, (reducerState: RouterReducerState<RouterState>) =>
    reducerState && reducerState.state.url),
  data: createSelector(routerFeature, (reducerState: RouterReducerState<RouterState>) =>
    reducerState && reducerState.state.data)
};
