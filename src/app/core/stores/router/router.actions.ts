import { Action } from '@ngrx/store';

import { type } from '../../../shared';
import { RouterData } from './router.model';

export const routerFeatureName = 'router';

export const RouterActionTypes = {
  GO: type(`[${routerFeatureName}] Go`),
  BACK: type(`[${routerFeatureName}] Back`),
  FORWARD: type(`[${routerFeatureName}] Forward`)
};

export class RouterGo implements Action {
  readonly type = RouterActionTypes.GO;
  constructor(public payload: RouterData) {}
}

export class RouterBack implements Action {
  readonly type = RouterActionTypes.BACK;
}

export class RouterForward implements Action {
  readonly type = RouterActionTypes.FORWARD;
}

export type RouterActions = RouterGo | RouterBack | RouterForward ;
