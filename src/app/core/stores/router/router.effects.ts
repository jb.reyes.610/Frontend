import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Location } from '@angular/common';

import { Actions, Effect, ofType } from '@ngrx/effects';

import { map, tap } from 'rxjs/operators';

import { RouterActionTypes, RouterGo } from './router.actions';
import { RouterData } from './router.model';

@Injectable()
export class RouterEffects {
  @Effect({ dispatch: false })
  routerGo$ = this.actions$.pipe(
    ofType(RouterActionTypes.GO),
    map((action: RouterGo) => action.payload),
    tap(({ path, query: queryParams, extras }: RouterData) => this.router.navigate(path, { queryParams, ...extras } ))
  );

  @Effect({ dispatch: false })
  routerBack$ = this.actions$.pipe(
    ofType(RouterActionTypes.BACK),
    tap(() => this.location.back())
  );

  @Effect({ dispatch: false })
  routerForward$ = this.actions$.pipe(
    ofType(RouterActionTypes.FORWARD),
    tap(() => this.location.forward())
  );

  constructor(
    private actions$: Actions,
    private location: Location,
    private router: Router) { }
}
