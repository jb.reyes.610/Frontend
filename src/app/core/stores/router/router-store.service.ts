import { Injectable } from '@angular/core';
import { NavigationExtras, Data, Params } from '@angular/router';

import { Store, select } from '@ngrx/store';
import { RouterReducerState } from '@ngrx/router-store';

import { Observable } from 'rxjs';

import { RouterGo, RouterBack, RouterForward } from './router.actions';
import { RouterState, routerSelectors } from './router.reducer';

@Injectable({
  providedIn: 'root'
})
export class RouterStore {
  state$: Observable<RouterReducerState<RouterState>> = this.store$.pipe(select(routerSelectors.state));

  data$: Observable<Data> = this.store$.pipe(select(routerSelectors.data));
  params$: Observable<Params> = this.store$.pipe(select(routerSelectors.params));
  queryParams$: Observable<Params> = this.store$.pipe(select(routerSelectors.queryParams));
  url$: Observable<string> = this.store$.pipe(select(routerSelectors.url));

  constructor(private store$: Store<RouterState>) { }

  go(path: any[], query?: any, extras?: NavigationExtras): void {
    this.store$.dispatch(new RouterGo({ path, query, extras }));
  }

  back(): void {
    this.store$.dispatch(new RouterBack());
  }

  forward(): void {
    this.store$.dispatch(new RouterForward());
  }
}
