import { NgModule } from '@angular/core';

import { EffectsModule } from '@ngrx/effects';
import { StoreRouterConnectingModule, RouterStateSerializer } from '@ngrx/router-store';

import { CustomSerializer } from './router.reducer';
import { RouterEffects } from './router.effects';
import { routerFeatureName } from './router.actions';

@NgModule({
  imports: [
    StoreRouterConnectingModule.forRoot({
      stateKey: routerFeatureName,
    }),
    EffectsModule.forFeature([RouterEffects]),
  ],
  providers: [
    [{ provide: RouterStateSerializer, useClass: CustomSerializer }]
  ]
})
export class RouterModule { }
