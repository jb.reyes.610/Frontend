import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';

import { HttpService, ConfigService, LoggerService } from './services';
import { httpInterceptorProviders } from './interceptors';
import { StoresModule } from './stores/stores.module';

@NgModule({
  imports: [
    StoresModule.forRoot()
  ],
  exports: [
    StoresModule
  ],
  providers: [
    ConfigService,
    HttpService,
    LoggerService,
    httpInterceptorProviders
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule?: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only');
    }
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CoreModule
    };
  }
}
