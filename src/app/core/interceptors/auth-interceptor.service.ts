import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpHeaders } from '@angular/common/http';

import { Observable } from 'rxjs';
import { mergeMap, map, take } from 'rxjs/operators';

import { AuthStore } from '../stores';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {
  constructor(private authStore: AuthStore) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const headers = {
      'Content-Type': 'application/json'
    };
    return this.authStore.state$.pipe(
      take(1),
      mergeMap(state => {
        if (state && state.authenticated && state.data) {
          headers['Authorization'] = `Bearer ${state.data.token}`;
        }
        const nextRequest = request.clone({
          headers: new HttpHeaders(headers)
        });
        return next.handle(nextRequest);
      })
    );
  }
}
