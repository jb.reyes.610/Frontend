import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent, HttpResponse } from '@angular/common/http';

import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

import { LoggerService } from '../services';

@Injectable({
  providedIn: 'root'
})
export class TimingInterceptor implements HttpInterceptor {
  constructor(private logger: LoggerService) { }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const started = Date.now();
    return next.handle(request).pipe(
      tap(event => {
        if (event instanceof HttpResponse) {
          const elapsed = Date.now() - started;
          this.logger.debug(`Request for ${request.urlWithParams} took ${elapsed} ms.`);
        }
      })
    );
  }
}
