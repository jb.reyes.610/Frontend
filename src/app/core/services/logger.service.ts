import { Injectable } from '@angular/core';

import { environment } from '../../../environments';
import { findKey } from '../../shared';

enum LogType {
  DEBUG = '#4CC355',
  INFO = '#10ADED',
  ERROR = '#FA113D',
  WARN = '#FB1'
}

@Injectable({
  providedIn: 'root'
})
export class LoggerService {
  debug(message: any): void {
    if (environment && environment.production) {
      return;
    }
    this.log(LogType.DEBUG, message);
  }

  error(message: any): void {
    this.log(LogType.ERROR, message);
  }

  info(message: any): void {
    this.log(LogType.INFO, message);
  }

  warn(message: any): void {
    this.log(LogType.WARN, message);
  }

  private log(type: LogType, message: any): void {
    console.log(`%c ${findKey(LogType, type)}:`, `color: ${type}; font-weight: bold`, message);
  }
}
