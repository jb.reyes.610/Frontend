import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Actions, ofType } from '@ngrx/effects';

import { Observable } from 'rxjs';
import { takeUntil, timeout, retry } from 'rxjs/operators';

import { AuthActionTypes } from '../stores/auth/auth.actions'; // TODO

enum Method { GET, POST, PUT, DELETE }

@Injectable({
  providedIn: 'root'
})
export class HttpService {
  constructor(
    private actions$: Actions,
    private http: HttpClient) { }

  mock(url: string, options?: any): Observable<any> {
    return this.request(Method.GET, url, options);
  }

  get(url: string, options?: any): Observable<any> {
    return this.request(Method.GET, url, options);
  }

  post(url: string, body: any, options?: any): Observable<any> {
    return this.request(Method.POST, url, body, options);
  }

  put(url: string, body: any, options?: any): Observable<any> {
    return this.request(Method.PUT, url, body, options);
  }

  delete(url: string, options?: any): Observable<any> {
    return this.request(Method.DELETE, url, options);
  }

  request(method: Method, url: string, body?: any, options?: any): Observable<any> {
    let httpRequest;
    switch (method) {
      case Method.GET:
        httpRequest = this.http.get(url, options);
        break;
      case Method.POST:
        httpRequest = this.http.post(url, body, options);
        break;
      case Method.PUT:
        httpRequest = this.http.put(url, body, options);
        break;
      case Method.DELETE:
        httpRequest = this.http.delete(url, options);
        break;
    }
    return httpRequest.pipe(
      timeout(10000),
      retry(2),
      takeUntil(this.actions$.pipe(ofType(AuthActionTypes.LOGOUT_SUCCESS)))
    );
  }
}
