import { Action } from '@ngrx/store';

import { type } from '../../shared';
import { Shape } from '../graphical-editor.model';

export const graphicalEditorFeatureName = 'graphicalEditor';

export const GraphicalEditorActionTypes = {
  CLEAR: type(`[${graphicalEditorFeatureName}] Clear`),
  CLEAR_FAILURE: type(`[${graphicalEditorFeatureName}] Clear Failure`),
  CLEAR_SUCCESS: type(`[${graphicalEditorFeatureName}] Clear Success`),
  SAVE: type(`[${graphicalEditorFeatureName}] Save`),
  SAVE_FAILURE: type(`[${graphicalEditorFeatureName}] Save Failure`),
  SAVE_SUCCESS: type(`[${graphicalEditorFeatureName}] Save Success`),
  UNDO: type(`[${graphicalEditorFeatureName}] Undo`),
  UNDO_FAILURE: type(`[${graphicalEditorFeatureName}] Undo Failure`),
  UNDO_SUCCESS: type(`[${graphicalEditorFeatureName}] Undo Success`),
};

export class GraphicalEditorClearAction implements Action {
  readonly type = GraphicalEditorActionTypes.CLEAR;
  constructor(public payload?: any) {}
}

export class GraphicalEditorClearFailureAction implements Action {
  readonly type = GraphicalEditorActionTypes.CLEAR_FAILURE;
  constructor(public payload?: any) {}
}

export class GraphicalEditorClearSuccessAction implements Action {
  readonly type = GraphicalEditorActionTypes.CLEAR_SUCCESS;
  constructor(public payload?: any) {}
}

export class GraphicalEditorSaveAction implements Action {
  readonly type = GraphicalEditorActionTypes.SAVE;
  constructor(public payload: Array<Shape>) {}
}

export class GraphicalEditorSaveFailureAction implements Action {
  readonly type = GraphicalEditorActionTypes.SAVE_FAILURE;
  constructor(public payload?: any) {}
}

export class GraphicalEditorSaveSuccessAction implements Action {
  readonly type = GraphicalEditorActionTypes.SAVE_SUCCESS;
  constructor(public payload?: any) {}
}

export class GraphicalEditorUndoAction implements Action {
  readonly type = GraphicalEditorActionTypes.UNDO;
  constructor(public payload?: any) {}
}

export class GraphicalEditorUndoFailureAction implements Action {
  readonly type = GraphicalEditorActionTypes.UNDO_FAILURE;
  constructor(public payload?: any) {}
}

export class GraphicalEditorUndoSuccessAction implements Action {
  readonly type = GraphicalEditorActionTypes.UNDO_SUCCESS;
  constructor(public payload?: any) {}
}

export type GraphicalEditorActions =
  GraphicalEditorClearAction | GraphicalEditorClearFailureAction | GraphicalEditorClearSuccessAction
  | GraphicalEditorSaveAction | GraphicalEditorSaveFailureAction | GraphicalEditorSaveSuccessAction
  | GraphicalEditorUndoAction | GraphicalEditorUndoFailureAction | GraphicalEditorUndoSuccessAction;
