import { TestBed, inject } from '@angular/core/testing';

import { GraphicalEditorStore } from './graphical-editor-store.service';

describe('GraphicalEditorStoreService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GraphicalEditorStore]
    });
  });

  it('should be created', inject([GraphicalEditorStore], (service: GraphicalEditorStore) => {
    expect(service).toBeTruthy();
  }));
});
