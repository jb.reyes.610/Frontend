import { Injectable } from '@angular/core';

import { Actions, Effect, ofType } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import {
  GraphicalEditorActionTypes,
  GraphicalEditorSaveAction,
  GraphicalEditorSaveSuccessAction,
  GraphicalEditorSaveFailureAction,
  GraphicalEditorClearAction,
  GraphicalEditorClearSuccessAction,
  GraphicalEditorClearFailureAction,
  GraphicalEditorUndoAction,
  GraphicalEditorUndoSuccessAction,
  GraphicalEditorUndoFailureAction
} from './graphical-editor.actions';

@Injectable()
export class GraphicalEditorEffects {
  @Effect()
  save$: Observable<Action> = this.actions$.pipe(
    ofType(GraphicalEditorActionTypes.SAVE),
    map((action: GraphicalEditorSaveAction) => new GraphicalEditorSaveSuccessAction(action.payload)),
    catchError(() => of(new GraphicalEditorSaveFailureAction({ code: 'GE001', message: 'Failed to save canvas.' })))
  );

  @Effect()
  clear$: Observable<Action> = this.actions$.pipe(
    ofType(GraphicalEditorActionTypes.CLEAR),
    map((action: GraphicalEditorClearAction) => new GraphicalEditorClearSuccessAction(action.payload)),
    catchError(() => of(new GraphicalEditorClearFailureAction({ code: 'GE002', message: 'Failed to clear canvas.' })))
  );

  @Effect()
  undo$: Observable<Action> = this.actions$.pipe(
    ofType(GraphicalEditorActionTypes.UNDO),
    map((action: GraphicalEditorUndoAction) => new GraphicalEditorUndoSuccessAction(action.payload)),
    catchError(() => of(new GraphicalEditorUndoFailureAction({ code: 'GE003', message: 'Failed to undo changes to canvas.' })))
  );

  constructor(private actions$: Actions) {}
}
