import { graphicalEditorInitialState, graphicalEditorReducer } from './graphical-editor.reducer';

describe('Graphical Editor Reducer', () => {
  describe('unknown action', () => {
    it('should return the initial state', () => {
      const action = {} as any;

      const result = graphicalEditorReducer(graphicalEditorInitialState, action);

      expect(result).toBe(graphicalEditorInitialState);
    });
  });
});
