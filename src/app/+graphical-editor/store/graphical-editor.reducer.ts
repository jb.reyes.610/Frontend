import { createFeatureSelector, createSelector } from '@ngrx/store';

import { Error } from '../../shared';
import { Shape } from '../graphical-editor.model';
import { GraphicalEditorActions, GraphicalEditorActionTypes, graphicalEditorFeatureName } from './graphical-editor.actions';

export interface GraphicalEditorState {
  loading: boolean;
  shapes: Array<Shape>;
  error?: Error;
}

export const graphicalEditorInitialState: GraphicalEditorState = {
  loading: false,
  shapes: []
};

export function graphicalEditorReducer(
  state: GraphicalEditorState = graphicalEditorInitialState,
  action: GraphicalEditorActions): GraphicalEditorState {

  switch (action.type) {
    case GraphicalEditorActionTypes.CLEAR:
      return {
        loading: true,
        ...state
      };
    case GraphicalEditorActionTypes.CLEAR_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case GraphicalEditorActionTypes.CLEAR_SUCCESS:
      return {
        ...state,
        loading: false,
        shapes: []
      };
    case GraphicalEditorActionTypes.SAVE:
      return {
        loading: true,
        ...state
      };
    case GraphicalEditorActionTypes.SAVE_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case GraphicalEditorActionTypes.SAVE_SUCCESS:
      return {
        ...state,
        loading: false,
        shapes: action.payload
      };
    case GraphicalEditorActionTypes.UNDO:
      return {
        loading: true,
        ...state
      };
    case GraphicalEditorActionTypes.UNDO_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.payload
      };
    case GraphicalEditorActionTypes.UNDO_SUCCESS:
      return {
        ...state,
        loading: false,
        shapes: action.payload
      };
    default:
      return state;
  }
}

export const graphicalEditorFeature = createFeatureSelector<GraphicalEditorState>(graphicalEditorFeatureName);

export const graphicalEditorSelectors = {
  state: graphicalEditorFeature,
  loading: createSelector(graphicalEditorFeature, (state: GraphicalEditorState) => state.loading),
  shapes: createSelector(graphicalEditorFeature, (state: GraphicalEditorState) => state.shapes),
  error: createSelector(graphicalEditorFeature, (state: GraphicalEditorState) => state.error)
};
