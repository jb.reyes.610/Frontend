import { Injectable } from '@angular/core';

import { Store, select } from '@ngrx/store';

import { Observable } from 'rxjs';

import { Error } from '../../shared';
import { Shape } from '../graphical-editor.model';
import { GraphicalEditorSaveAction, GraphicalEditorUndoAction, GraphicalEditorClearAction } from './graphical-editor.actions';
import { GraphicalEditorState, graphicalEditorSelectors } from './graphical-editor.reducer';

@Injectable({
  providedIn: 'root'
})
export class GraphicalEditorStore {
  state$: Observable<GraphicalEditorState> = this.store$.pipe(select(graphicalEditorSelectors.state));

  loading$: Observable<boolean> = this.store$.pipe(select(graphicalEditorSelectors.loading));
  shapes$: Observable<Array<Shape>> = this.store$.pipe(select(graphicalEditorSelectors.shapes));
  error$: Observable<Error> = this.store$.pipe(select(graphicalEditorSelectors.error));

  constructor(private store$: Store<GraphicalEditorState>) { }

  save(shapes: Array<Shape>): Observable<Array<Shape>> {
    this.store$.dispatch(new GraphicalEditorSaveAction(shapes));
    return this.shapes$;
  }

  undo(shapes: Array<Shape>): Observable<Array<Shape>> {
    this.store$.dispatch(new GraphicalEditorUndoAction(shapes));
    return this.shapes$;
  }

  clear(): Observable<Array<Shape>> {
    this.store$.dispatch(new GraphicalEditorClearAction());
    return this.shapes$;
  }
}
