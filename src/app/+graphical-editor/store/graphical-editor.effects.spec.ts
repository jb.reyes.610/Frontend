import { TestBed } from '@angular/core/testing';

import { provideMockActions } from '@ngrx/effects/testing';

import { Observable, of } from 'rxjs';

import { GraphicalEditorEffects } from './graphical-editor.effects';

describe('GraphicalEditorEffects', () => {
  const actions$: Observable<any> = of();
  let effects: GraphicalEditorEffects;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        GraphicalEditorEffects,
        provideMockActions(() => actions$)
      ]
    });

    effects = TestBed.get(GraphicalEditorEffects);
  });

  it('should be created', () => {
    expect(effects).toBeTruthy();
  });
});
