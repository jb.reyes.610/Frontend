import { Injectable, ElementRef } from '@angular/core';

import { Shape, Square, Circle, Triangle, Line, Shapes, Coordinates } from './graphical-editor.model';
import { getAnchorPoints, calculateDistance, getResizingShapes, getMouseCursors } from './graphical-editor.util';

export const RESIZE_ANCHOR_RADIUS = 4;
export const DEFAULT_DIMENSIONS = 140;
export const SHAPES: { [key: string]: Square | Circle | Triangle | Line } = {
  square: { name: 'square', xPos: 0, yPos: 0, width: 140, height: 140 },
  triangle: { name: 'triangle', xPos: 70, yPos: 0, points: [ { xPos: 140, yPos: 140 }, { xPos: 0, yPos: 140 } ] },
  circle: { name: 'circle', xPos: 70, yPos: 70, radius: 70 },
  line: { name: 'line', xPos: 0, yPos: 35, xPosEnd: 140, yPosEnd: 105 }
};

@Injectable({
  providedIn: 'root'
})
export class GraphicalEditorService {
  drawShape(context: CanvasRenderingContext2D, shape: Shape): Shape {
    let newShape: Shape;
    if (context && shape) {
      context.beginPath();
      switch (shape.name) {
        case 'square':
          newShape = this.drawSquare(context, shape as Square);
          break;
        case 'circle':
          newShape = this.drawCircle(context, shape as Circle);
          break;
        case 'triangle':
          newShape = this.drawTriangle(context, shape as Triangle);
          break;
        case 'line':
          newShape = this.drawLine(context, shape as Line);
          break;
      }
      context.stroke();
      context.closePath();
      context.save();
    }
    return newShape;
  }

  drawSquare(context: CanvasRenderingContext2D, square: Square): Square {
    const newSquare: Square = {
      ...square,
      xPos: square.xPos + this.getXPosOffset(square),
      yPos: square.yPos + this.getYPosOffset(square)
    };
    this.resetOffsets(newSquare);
    context.rect(newSquare.xPos, newSquare.yPos, newSquare.width, newSquare.height);
    return newSquare;
  }

  drawCircle(context: CanvasRenderingContext2D, circle: Circle): Circle {
    const newCircle: Circle = {
      ...circle,
      xPos: circle.xPos + this.getXPosOffset(circle),
      yPos: circle.yPos + this.getYPosOffset(circle)
    };
    this.resetOffsets(newCircle);
    context.arc(newCircle.xPos, newCircle.yPos, newCircle.radius, 0, 2 * Math.PI);
    return newCircle;
  }

  drawTriangle(context: CanvasRenderingContext2D, triangle: Triangle): Triangle {
    const newTriangle: Triangle = {
      ...triangle,
      xPos: triangle.xPos + this.getXPosOffset(triangle),
      yPos: triangle.yPos + this.getYPosOffset(triangle),
      points: [
        {
          xPos: triangle.points[0].xPos + this.getXPosOffset(triangle),
          yPos: triangle.points[0].yPos + this.getYPosOffset(triangle)
        },
        {
          xPos: triangle.points[1].xPos + this.getXPosOffset(triangle),
          yPos: triangle.points[1].yPos + this.getYPosOffset(triangle)
        },
      ]
    };
    this.resetOffsets(newTriangle);
    context.moveTo(newTriangle.xPos, newTriangle.yPos);
    if (newTriangle.points) {
      newTriangle.points.forEach(point => {
        context.lineTo(point.xPos, point.yPos);
      });
    }
    context.lineTo(newTriangle.xPos, newTriangle.yPos);
    return newTriangle;
  }

  drawLine(context: CanvasRenderingContext2D, line: Line): Line {
    const xPosOffset = this.getXPosOffset(line);
    const yPosOffset = this.getYPosOffset(line);
    const newLine: Line = {
      ...line,
      xPos: line.xPos + xPosOffset,
      yPos: line.yPos + yPosOffset,
      xPosEnd: line.xPosEnd + xPosOffset,
      yPosEnd: line.yPosEnd + yPosOffset
    };
    this.resetOffsets(newLine);
    context.moveTo(newLine.xPos, newLine.yPos);
    context.lineTo(newLine.xPosEnd, newLine.yPosEnd);
    return newLine;
  }

  getShapeAnchorPoints(shape: Shape): Array<Coordinates> {
    return getAnchorPoints(shape);
  }

  addResizeAnchorPoints(context: CanvasRenderingContext2D, shape: Shape): void {
    if (!shape) {
      return;
    }
    this.getShapeAnchorPoints(shape).forEach(anchorPoint => this.addAnchorPoint(context, anchorPoint.xPos, anchorPoint. yPos));
  }

  addAnchorPoint(context: CanvasRenderingContext2D, xPos: number, yPos: number,
    radius: number = this.getResizeAnchorRadius()): void {

    context.fillStyle = '#fff';
    context.beginPath();
    context.arc(xPos, yPos, radius, 0, 2 * Math.PI);
    context.fill();
    context.closePath();
    context.stroke();
    context.save();
  }

  isWithinAnchorPoint(xPos: number, yPos: number, xPosAnchor: number, yPosAnchor: number,
    radius: number = this.getResizeAnchorRadius()): boolean {

    return this.calculateDistance(xPos, yPos, xPosAnchor, yPosAnchor) <= radius;
  }

  isWithinShapeAnchorPoints(xPos: number, yPos: number, shape: Shape): boolean {
    return this.getAnchorPoint(xPos, yPos, shape) !== -1;
  }

  getAnchorPoint(xPosMouse: number, yPosMouse: number, shape: Shape): number {
    if (!shape) {
      return -1;
    }

    let searchedIndex: number;
    const searchedAnchorPoint = this.getShapeAnchorPoints(shape).find((anchorPoint, index) => {
      searchedIndex = index;
      return this.isWithinAnchorPoint(xPosMouse, yPosMouse, anchorPoint.xPos, anchorPoint.yPos);
    });
    if (!searchedAnchorPoint) {
      searchedIndex = -1;
    }
    return searchedIndex;
  }

  drawResizingShape(context: CanvasRenderingContext2D,
    activeShape: Shape, anchorPoint: number, xPosMouse: number, yPosMouse: number): Shape {

    if (!context || !activeShape || anchorPoint === -1) {
      return undefined;
    }

    const resizingShape: Shape = getResizingShapes(activeShape, xPosMouse, yPosMouse)[anchorPoint];
    this.drawShape(context, resizingShape);
    this.addResizeAnchorPoints(context, resizingShape);
    return resizingShape;
  }

  getMouseCursor(shape: Shape, anchorPoint: number): string {
    return getMouseCursors(shape)[anchorPoint];
  }

  getCanvas(element: ElementRef): HTMLCanvasElement {
    if (!element) {
      return undefined;
    }
    return element.nativeElement;
  }

  getContext(canvas: HTMLCanvasElement): CanvasRenderingContext2D {
    if (!canvas) {
      return undefined;
    }
    return canvas.getContext('2d');
  }

  calculateDistance(xPos1: number, yPos1: number, xPos2: number, yPos2: number): number {
    return calculateDistance(xPos1, yPos1, xPos2, yPos2);
  }

  getXPosOffset(shape: Shape): number {
    if (shape) {
      return (shape.xPosOffsetTgt || 0) - (shape.xPosOffsetSrc || 0);
    }
    return 0;
  }

  getYPosOffset(shape: Shape): number {
    if (shape) {
      return (shape.yPosOffsetTgt || 0) - (shape.yPosOffsetSrc || 0);
    }
    return 0;
  }

  resetOffsets(shape: Shape): void {
    if (shape) {
      delete shape.xPosOffsetSrc;
      delete shape.xPosOffsetTgt;
      delete shape.yPosOffsetSrc;
      delete shape.yPosOffsetTgt;
    }
  }

  getResizeAnchorRadius(): number {
    return RESIZE_ANCHOR_RADIUS;
  }

  getDefaultDimensions(): number {
    return DEFAULT_DIMENSIONS;
  }

  getShapes(): Shapes {
    return SHAPES;
  }
}
