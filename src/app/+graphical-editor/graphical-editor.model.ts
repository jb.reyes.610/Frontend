export type ShapeType = 'square' | 'circle' | 'triangle' | 'line';

export interface Coordinates {
  xPos: number;
  yPos: number;
}

export interface Shape extends Coordinates {
  name: ShapeType;
  xPosOffsetSrc?: number;
  yPosOffsetSrc?: number;
  xPosOffsetTgt?: number;
  yPosOffsetTgt?: number;
}

export interface Shapes {
  [key: string]: Square | Circle | Triangle | Line | Shape | any;
}

export interface Sided {
  length: number;
}

export interface MultiSided {
  points: Array<Coordinates>;
}

export interface Square extends Shape {
  width: number;
  height: number;
}

export interface Circle extends Shape {
  radius: number;
}

export interface Triangle extends Shape, MultiSided { }

export interface Line extends Shape {
  xPosEnd: number;
  yPosEnd: number;
}
