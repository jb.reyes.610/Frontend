import { Shape, Square, Circle, Triangle, Line, Coordinates } from './graphical-editor.model';

export const calculateDistance = (xPos1: number, yPos1: number, xPos2: number, yPos2: number): number => {
  return Math.sqrt(Math.pow(xPos2 - xPos1, 2) + Math.pow(yPos2 - yPos1, 2));
};

export const getMouseCursors = (shape: Shape): Array<string> => {
  switch (shape.name) {
    case 'square':
      return ['nw-resize', 'ne-resize', 'se-resize', 'sw-resize', 'move'];
    case 'triangle':
      return ['n-resize', 'se-resize', 'sw-resize', 'move'];
    case 'circle':
      return ['n-resize', 'e-resize', 's-resize', 'w-resize', 'move'];
    case 'line':
      return ['n-resize', 's-resize', 'move'];
  }
};

export const getAnchorPoints = (shape: Shape): Array<Coordinates> => {
  if (!shape) {
    return [];
  }
  switch (shape.name) {
    case 'square':
      return [
        { xPos: shape.xPos, yPos: shape.yPos },
        { xPos: shape.xPos + (shape as Square).width, yPos: shape.yPos },
        { xPos: shape.xPos + (shape as Square).width, yPos: shape.yPos + (shape as Square).height },
        { xPos: shape.xPos, yPos: shape.yPos + (shape as Square).height },
        { xPos: shape.xPos + (shape as Square).width / 2, yPos: shape.yPos + (shape as Square).height / 2 }
      ];
    case 'circle':
      return [
        { xPos: shape.xPos, yPos: shape.yPos - (shape as Circle).radius },
        { xPos: shape.xPos + (shape as Circle).radius, yPos: shape.yPos },
        { xPos: shape.xPos, yPos: shape.yPos + (shape as Circle).radius },
        { xPos: shape.xPos - (shape as Circle).radius, yPos: shape.yPos },
        { xPos: shape.xPos, yPos: shape.yPos }
      ];
    case 'triangle':
      return [
        { xPos: shape.xPos, yPos: shape.yPos },
        { xPos: (shape as Triangle).points[0].xPos, yPos: (shape as Triangle).points[0].yPos },
        { xPos: (shape as Triangle).points[1].xPos, yPos: (shape as Triangle).points[1].yPos },
        {
          xPos: (shape.xPos + (shape as Triangle).points[0].xPos + (shape as Triangle).points[1].xPos) / 3,
          yPos: (shape.yPos + (shape as Triangle).points[0].yPos + (shape as Triangle).points[1].yPos) / 3
        }
      ];
    case 'line':
      return [
        { xPos: shape.xPos, yPos: shape.yPos },
        { xPos: (shape as Line).xPosEnd, yPos: (shape as Line).yPosEnd },
        { xPos: (shape.xPos + (shape as Line).xPosEnd) / 2, yPos: (shape.yPos + (shape as Line).yPosEnd) / 2 }
      ];
  }
};

export const getResizingShapes = (shape: Shape, xPosMouse: number, yPosMouse: number) => {
  switch (shape.name) {
    case 'square':
      return [
        {
          ...shape, xPos: xPosMouse, yPos: yPosMouse,
          width: calculateDistance(xPosMouse, shape.yPos, shape.xPos + (shape as Square).width, shape.yPos),
          height: calculateDistance(shape.xPos, yPosMouse, shape.xPos, shape.yPos + (shape as Square).height)
        },
        {
          ...shape, yPos: yPosMouse,
          width: calculateDistance(shape.xPos, shape.yPos, xPosMouse, shape.yPos),
          height: calculateDistance(shape.xPos, shape.yPos + (shape as Square).height, shape.xPos, yPosMouse)
        },
        {
          ...shape,
          width: calculateDistance(shape.xPos, shape.yPos, xPosMouse, shape.yPos),
          height: calculateDistance(shape.xPos, shape.yPos, shape.xPos, yPosMouse)
        },
        {
          ...shape, xPos: xPosMouse,
          width: calculateDistance(xPosMouse, shape.yPos, shape.xPos + (shape as Square).width, shape.yPos),
          height: calculateDistance(shape.xPos, shape.yPos, shape.xPos, yPosMouse)
        },
        { ...shape, xPos: xPosMouse - (shape as Square).width / 2, yPos: yPosMouse - (shape as Square).height / 2 }
      ];
    case 'circle':
      return [
        {
          ...shape,
          yPos: yPosMouse + (calculateDistance(shape.xPos, shape.yPos + (shape as Circle).radius, shape.xPos, yPosMouse) / 2),
          radius: calculateDistance(shape.xPos, shape.yPos + (shape as Circle).radius, shape.xPos, yPosMouse) / 2
        },
        {
          ...shape,
          xPos: xPosMouse - calculateDistance(shape.xPos - (shape as Circle).radius, shape.yPos, xPosMouse, shape.yPos) / 2,
          radius: calculateDistance(shape.xPos - (shape as Circle).radius, shape.yPos, xPosMouse, shape.yPos) / 2
        },
        {
          ...shape,
          yPos: yPosMouse - calculateDistance(shape.xPos, shape.yPos - (shape as Circle).radius, shape.xPos, yPosMouse) / 2,
          radius: calculateDistance(shape.xPos, shape.yPos - (shape as Circle).radius, shape.xPos, yPosMouse) / 2
        },
        {
          ...shape,
          xPos: xPosMouse + (calculateDistance(shape.xPos + (shape as Circle).radius, shape.yPos, xPosMouse, shape.yPos) / 2),
          radius: calculateDistance(shape.xPos + (shape as Circle).radius, shape.yPos, xPosMouse, shape.yPos) / 2
        },
        { ...shape, xPos: xPosMouse, yPos: yPosMouse }
      ];
    case 'triangle':
      const bottomLength = calculateDistance(
        (shape as Triangle).points[0].xPos,
        (shape as Triangle).points[0].yPos,
        (shape as Triangle).points[1].xPos,
        (shape as Triangle).points[1].yPos);
      const xPosMedian = (shape.xPos + (shape as Triangle).points[0].xPos + (shape as Triangle).points[1].xPos) / 3;
      const yPosMedian = (shape.yPos + (shape as Triangle).points[0].yPos + (shape as Triangle).points[1].yPos) / 3;
      return [
        { ...shape, yPos: yPosMouse },
        {
          ...shape, xPos: xPosMouse - bottomLength / 2,
          points: [
            {
              xPos: xPosMouse,
              yPos: yPosMouse
            },
            {
              xPos: (shape as Triangle).points[1].xPos,
              yPos: yPosMouse
            }
          ]
        },
        {
          ...shape, xPos: xPosMouse + bottomLength / 2,
          points: [
            {
              xPos: (shape as Triangle).points[0].xPos,
              yPos: yPosMouse
            },
            {
              xPos: xPosMouse,
              yPos: yPosMouse
            }
          ]
        },
        {
          ...shape,
          xPos: xPosMouse,
          yPos: yPosMouse - calculateDistance(0, shape.yPos, 0, yPosMedian),
          points: [
            {
              xPos: xPosMouse + bottomLength / 2,
              yPos: yPosMouse + calculateDistance(0, (shape as Triangle).points[0].yPos, 0, yPosMedian)
            },
            {
              xPos: xPosMouse - bottomLength / 2,
              yPos: yPosMouse + calculateDistance(0, (shape as Triangle).points[1].yPos, 0, yPosMedian)
            }
          ]
        }
      ];
    case 'line':
      return [
        { ...shape, xPos: xPosMouse, yPos: yPosMouse },
        { ...shape, xPosEnd: xPosMouse, yPosEnd: yPosMouse },
        {
          ...shape,
          xPos: xPosMouse - ((shape as Line).xPosEnd - shape.xPos) / 2,
          yPos: yPosMouse - ((shape as Line).yPosEnd - shape.yPos) / 2,
          xPosEnd: xPosMouse + ((shape as Line).xPosEnd - shape.xPos) / 2,
          yPosEnd: yPosMouse + ((shape as Line).yPosEnd - shape.yPos) / 2
        }
      ];
  }
};

