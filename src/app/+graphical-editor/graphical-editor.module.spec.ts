import { GraphicalEditorModule } from './graphical-editor.module';

describe('GraphicalEditorModule', () => {
  let graphicalEditorModule: GraphicalEditorModule;

  beforeEach(() => {
    graphicalEditorModule = new GraphicalEditorModule();
  });

  it('should create an instance', () => {
    expect(graphicalEditorModule).toBeTruthy();
  });
});
