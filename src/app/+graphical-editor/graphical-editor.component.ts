import { Component, ChangeDetectionStrategy, EventEmitter, OnDestroy, OnInit, ViewChild } from '@angular/core';

import { Subscription } from 'rxjs';

import { Button } from '../shared';
import { RouterStore } from '../core';
import { CanvasComponent } from './canvas';
import { Shape } from './graphical-editor.model';

@Component({
  selector: 'app-graphical-editor',
  templateUrl: './graphical-editor.component.html',
  styleUrls: ['./graphical-editor.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class GraphicalEditorComponent implements OnDestroy, OnInit {
  @ViewChild('canvas') canvasComponent: CanvasComponent;

  save: EventEmitter<any>;
  undo: EventEmitter<any>;
  clear: EventEmitter<any>;

  pageButtons: Array<Button>;
  subscriptions: Subscription;

  constructor(public routerStore: RouterStore) {
    this.subscriptions = new Subscription();

    this.save = new EventEmitter<any>();
    this.undo = new EventEmitter<any>();
    this.clear = new EventEmitter<any>();

    this.pageButtons = [
      { color: 'primary', cmClick: this.save, label: 'Save', icon: 'save', type: 'icon' },
      { color: 'primary', cmClick: this.undo, label: 'Undo', icon: 'undo', type: 'icon' },
      { color: 'primary', cmClick: this.clear, label: 'Clear', icon: 'delete', type: 'icon' }
    ];
  }

  ngOnInit() {
    this.subscriptions.add(this.undo.subscribe(() => this.canvasComponent.undo()));
    this.subscriptions.add(this.clear.subscribe(() => this.canvasComponent.clearCanvas(true)));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onShapesChanged(shapes: Array<Shape>): void {
    if (!shapes || shapes.length === 0) {
      this.pageButtons[1].disabled = true;
    } else {
      this.pageButtons[1].disabled = false;
    }
    this.pageButtons = [...this.pageButtons];
  }
}
