import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnInit, ViewChild } from '@angular/core';

import { GraphicalEditorService } from '../graphical-editor.service';
import { Shapes } from '../graphical-editor.model';

@Component({
  selector: 'app-toolbox',
  templateUrl: './toolbox.component.html',
  styleUrls: ['./toolbox.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ToolboxComponent implements OnInit, AfterViewInit {
  @ViewChild('square') squareElement: ElementRef;
  @ViewChild('triangle') triangleElement: ElementRef;
  @ViewChild('circle') circleElement: ElementRef;
  @ViewChild('line') lineElement: ElementRef;

  squareCanvas: HTMLCanvasElement;
  triangleCanvas: HTMLCanvasElement;
  circleCanvas: HTMLCanvasElement;
  lineCanvas: HTMLCanvasElement;

  shapes: Shapes;

  constructor(private graphicalEditor: GraphicalEditorService) {
    this.shapes = this.graphicalEditor.getShapes();
  }

  ngOnInit() {
    this.squareCanvas = this.graphicalEditor.getCanvas(this.squareElement);
    this.triangleCanvas = this.graphicalEditor.getCanvas(this.triangleElement);
    this.circleCanvas = this.graphicalEditor.getCanvas(this.circleElement);
    this.lineCanvas = this.graphicalEditor.getCanvas(this.lineElement);
  }

  ngAfterViewInit() {
    this.initCanvas();
    this.drawShapes();
  }

  initCanvas(): void {
    this.setDefaultDimensions(this.squareCanvas);
    this.setDefaultDimensions(this.triangleCanvas);
    this.setDefaultDimensions(this.circleCanvas);
    this.setDefaultDimensions(this.lineCanvas);
  }

  drawShapes(): void {
    this.graphicalEditor.drawShape(
      this.graphicalEditor.getContext(this.squareCanvas),
      this.shapes.square);
    this.graphicalEditor.drawShape(
      this.graphicalEditor.getContext(this.triangleCanvas),
      this.shapes.triangle);
    this.graphicalEditor.drawShape(
      this.graphicalEditor.getContext(this.circleCanvas),
      this.shapes.circle);
    this.graphicalEditor.drawShape(
      this.graphicalEditor.getContext(this.lineCanvas),
      this.shapes.line);
  }

  setDefaultDimensions(canvas: HTMLCanvasElement): void {
    canvas.width = this.graphicalEditor.getDefaultDimensions();
    canvas.height = this.graphicalEditor.getDefaultDimensions();
  }
}
