import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GraphicalEditorComponent } from './graphical-editor.component';

const routes: Routes = [
  { path: '', component: GraphicalEditorComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class GraphicalEditorRoutingModule { }
