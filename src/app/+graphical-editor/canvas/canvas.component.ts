import {
  AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, EventEmitter, HostListener, OnInit, ViewChild, Output
} from '@angular/core';

import { LoggerService } from '../../core';
import { Shape } from '../graphical-editor.model';
import { GraphicalEditorService } from '../graphical-editor.service';

@Component({
  selector: 'app-canvas',
  templateUrl: './canvas.component.html',
  styleUrls: ['./canvas.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CanvasComponent implements OnInit, AfterViewInit {
  @ViewChild('canvas') canvasElement: ElementRef;

  @Output() shapesChanged: EventEmitter<Array<Shape>>;

  hostElement: HTMLElement;
  canvas: HTMLCanvasElement;
  context: CanvasRenderingContext2D;

  shapes: Array<Shape>;
  activeShape: Shape;
  drawingLoop: any;
  editMode: boolean;
  resizing: boolean;
  xPosMouse: number;
  yPosMouse: number;

  constructor(
    private hostElementRef: ElementRef,
    private graphicalEditor: GraphicalEditorService,
    private logger: LoggerService) {

    this.shapes = [];
    this.editMode = false;
    this.resizing = false;
    this.shapesChanged = new EventEmitter<Array<Shape>>();
  }

  @HostListener('window:resize')
  onResize() {
    this.addActiveShape();
    this.resetCanvas();
  }

  @HostListener('window:mousedown', ['$event'])
  onMouseDown(event: MouseEvent) {
    if (event.target !== this.canvas) {
      this.addActiveShape();
    }
  }

  @HostListener('document:keydown.escape')
  onKeyDownEscape() {
    this.addActiveShape();
  }

  ngOnInit() {
    this.hostElement = this.hostElementRef.nativeElement as HTMLElement;
    this.canvas = this.graphicalEditor.getCanvas(this.canvasElement);
    this.context = this.graphicalEditor.getContext(this.canvas);

    this.initCanvasMouseDownListener();
    this.initCanvasMouseUpListener();
    this.initCanvasMouseMoveListener();
    this.emitShapes();
  }

  ngAfterViewInit() {
    this.resetCanvas();
  }

  resetCanvas(): void {
    this.canvas.width = this.hostElement.clientWidth;
    this.canvas.height = this.hostElement.clientHeight;
    this.redrawShapes(this.shapes);
  }

  redrawShapes(shapes: Array<Shape>): void {
    if (!shapes) {
      return;
    }
    shapes.forEach(shape => this.graphicalEditor.drawShape(this.context, shape));
  }

  onDrop(data: any): void {
    if (!data) {
      return;
    }
    this.activeShape = this.graphicalEditor.drawShape(this.context, data);
    this.graphicalEditor.addResizeAnchorPoints(this.context, this.activeShape);
    this.editMode = true;
  }

  clearCanvas(forced?: boolean): void {
    if (forced) {
      this.resizing = false;
      this.editMode = false;
      this.shapes = [];
      this.emitShapes();
    }
    this.context.clearRect(0, 0, this.hostElement.clientWidth, this.hostElement.clientHeight);
  }

  undo(): void {
    this.resizing = false;
    this.editMode = false;
    this.clearCanvas();
    this.shapes.pop();
    this.emitShapes();
    this.redrawShapes(this.shapes);
  }

  private addActiveShape(): void {
    if (!this.editMode) {
      return;
    }

    this.stopDrawingLoop();
    this.editMode = false;
    this.clearCanvas();
    if (this.activeShape) {
      this.shapes.push(this.activeShape);
      this.emitShapes();
      this.activeShape = undefined;
    }
    this.redrawShapes(this.shapes);
  }

  private initCanvasMouseDownListener(): void {
    this.canvas.addEventListener('mousedown', event => {
      if (this.editMode) {
        this.xPosMouse = event.offsetX;
        this.yPosMouse = event.offsetY;
        if (!this.graphicalEditor.isWithinShapeAnchorPoints(this.xPosMouse, this.yPosMouse, this.activeShape)) {
          this.addActiveShape();
          return;
        }

        this.resizing = true;
        this.runDrawingLoop(
          this.graphicalEditor.getAnchorPoint(
            this.xPosMouse, this.yPosMouse, this.activeShape
        ));
      }
    });
  }

  private emitShapes(): void {
    this.shapesChanged.emit([...this.shapes]);
  }

  private initCanvasMouseUpListener(): void {
    this.canvas.addEventListener('mouseup', event => {
      this.stopDrawingLoop();
      this.resizing = false;
    });
  }

  private initCanvasMouseMoveListener(): void {
    this.canvas.addEventListener('mousemove', event => {
      if (!this.editMode) {
        return;
      }
      if (this.resizing) {
        this.xPosMouse = event.offsetX;
        this.yPosMouse = event.offsetY;
      }
      if (this.graphicalEditor.isWithinShapeAnchorPoints(event.offsetX, event.offsetY, this.activeShape)) {
        const anchorPoint = this.graphicalEditor.getAnchorPoint(event.offsetX, event.offsetY, this.activeShape);
        this.canvas.style.cursor = this.graphicalEditor.getMouseCursor(this.activeShape, anchorPoint);
      } else if (!this.resizing) {
        this.canvas.style.cursor = 'default';
      }
    });
  }

  private runDrawingLoop(firstClickedAnchorPoint: number): void {
    this.drawingLoop = setInterval(() => {
      this.logger.debug('Drawing loop running...');
      this.clearCanvas();
      this.redrawShapes(this.shapes);
      this.activeShape = this.graphicalEditor.drawResizingShape(
        this.context, this.activeShape, firstClickedAnchorPoint, this.xPosMouse, this.yPosMouse
      );
    }, 30);
  }

  private stopDrawingLoop(): void {
    if (this.drawingLoop) {
      this.logger.debug('Drawing loop stopped!');
      clearInterval(this.drawingLoop);
      this.drawingLoop = undefined;
    }
  }
}
