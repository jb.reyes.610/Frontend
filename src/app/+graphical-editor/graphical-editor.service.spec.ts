import { TestBed, inject } from '@angular/core/testing';

import { GraphicalEditorService } from './graphical-editor.service';

describe('GraphicalEditorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [GraphicalEditorService]
    });
  });

  it('should be created', inject([GraphicalEditorService], (service: GraphicalEditorService) => {
    expect(service).toBeTruthy();
  }));
});
