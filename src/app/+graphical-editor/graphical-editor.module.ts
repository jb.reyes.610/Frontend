import { NgModule } from '@angular/core';

import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';

import { DragDropService, SharedModule } from '../shared';

import { GraphicalEditorEffects, GraphicalEditorStore, graphicalEditorFeatureName, graphicalEditorReducer } from './store';
import { ToolboxComponent } from './toolbox';
import { CanvasComponent } from './canvas';
import { GraphicalEditorRoutingModule } from './graphical-editor-routing.module';
import { GraphicalEditorService } from './graphical-editor.service';
import { GraphicalEditorComponent } from './graphical-editor.component';

@NgModule({
  imports: [
    SharedModule,
    GraphicalEditorRoutingModule,
    StoreModule.forFeature(graphicalEditorFeatureName, graphicalEditorReducer),
    EffectsModule.forFeature([GraphicalEditorEffects]),
  ],
  declarations: [
    GraphicalEditorComponent,
    ToolboxComponent,
    CanvasComponent
  ],
  providers: [
    GraphicalEditorStore,
    DragDropService,
    GraphicalEditorService
  ]
})
export class GraphicalEditorModule { }
