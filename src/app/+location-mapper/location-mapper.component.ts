import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { AgmMap } from '@agm/core';

import { Subscription } from 'rxjs';

import { LoggerService, RouterStore } from '../core';

import { GridMap, Location, Locations, Routes } from './location-mapper.model';
import { LocationMapperService } from './location-mapper.service';

@Component({
  selector: 'app-location-mapper',
  templateUrl: './location-mapper.component.html',
  styleUrls: ['./location-mapper.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LocationMapperComponent implements AfterViewInit, OnInit, OnDestroy {
  @ViewChild('routes') routesCanvasElement: ElementRef;
  @ViewChild('markers') markersCanvasElement: ElementRef;
  @ViewChild('container') containerElement: ElementRef;

  subscriptions: Subscription = new Subscription();

  routesCanvas: HTMLCanvasElement;
  markersCanvas: HTMLCanvasElement;
  routesContext: CanvasRenderingContext2D;
  markersContext: CanvasRenderingContext2D;

  center: GridMap;
  locations: Locations;
  locationMap: Array<Location>;
  routes: Routes;

  routeForm: FormGroup;
  origin: FormControl;
  destination: FormControl;

  possibleRoutes: Array<Array<Location>> = [];
  routeFound: Array<Location>;

  constructor(
    public routerStore: RouterStore,
    private formBuilder: FormBuilder,
    private locationMapper: LocationMapperService,
    private logger: LoggerService) {

    this.center = this.locationMapper.getCenter();
    this.locations = this.locationMapper.getLocations();
    this.locationMap = this.locationMapper.getSortedLocations();
    this.routes = this.locationMapper.getRoutes();

    this.routeForm = this.formBuilder.group({
      origin: '',
      destination: ''
    });
    this.origin = this.routeForm.get('origin') as FormControl;
    this.destination = this.routeForm.get('destination') as FormControl;
  }

  ngOnInit() {
    this.subscriptions.add(this.origin.valueChanges.subscribe(() => this.markRoute()));
    this.subscriptions.add(this.destination.valueChanges.subscribe(() => this.markRoute()));
  }

  ngAfterViewInit() {
    this.routesCanvas = this.routesCanvasElement.nativeElement;
    this.markersCanvas = this.markersCanvasElement.nativeElement;

    this.resetCanvasDimensions(this.routesCanvas);
    this.resetCanvasDimensions(this.markersCanvas);
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onMapReady(map: AgmMap): void {
    map.clickableIcons = false;
    map.disableDoubleClickZoom = true;
    map.draggable = false;
    map.keyboardShortcuts = false;
    map.fullscreenControl = false;
    map.mapTypeControl = false;
    map.panControl = false;
    map.rotateControl = false;
    map.scaleControl = false;
    map.scrollwheel = false;
    map.streetViewControl = false;
    map.usePanning = false;
    map.zoomControl = false;

    this.initRoutes();
    this.markersContext = this.markersCanvas.getContext('2d');
    this.markersContext.strokeStyle = '#ff0000';
    this.markersContext.fillStyle = '#ff0000';
    this.markersContext.lineWidth = 1;
  }

  initRoutes(): void {
    this.routesContext = this.routesCanvas.getContext('2d');
    this.routesContext.strokeStyle = '#0000ff';
    this.routesContext.setLineDash([1, 2]);
    this.routesContext.beginPath();

    const routeOrigins = this.locationMapper.getRouteKeys();
    const routedLocations: Array<Location> = [];
    routeOrigins.forEach(origin => {
      const originLocation = this.locations[origin];
      const originDestinations: Array<Location> = this.routes[origin];
      originDestinations.forEach(destinationLocation => {
        if (!this.locationMapper.containsLocation(routedLocations, destinationLocation)) {
          this.locationMapper.connectLocation(this.routesContext, originLocation, destinationLocation);
        }
      });
      routedLocations.push(originLocation);
    });
    this.routesContext.closePath();
    this.routesContext.stroke();
    this.routesContext.save();
  }

  clearMapCanvasMarker(): void {
    this.markersContext.clearRect(0, 0, this.containerElement.nativeElement.clientWidth, this.containerElement.nativeElement.clientHeight);
  }

  resetCanvasDimensions(canvasElement: HTMLCanvasElement): void {
    if (canvasElement) {
      canvasElement.width = this.containerElement.nativeElement.clientWidth;
      canvasElement.height = this.containerElement.nativeElement.clientHeight;
    }
  }

  private markRoute(): void {
    if (!this.origin.value || !this.destination.value
      || (typeof this.origin.value !== 'object')
      || (typeof this.destination.value !== 'object')) {

      this.clearMapCanvasMarker();
      return;
    }

    const originLocation = this.origin.value as Location;
    const destinationLocation = this.destination.value as Location;

    this.routeFound = undefined;
    this.findRoute(destinationLocation, originLocation);
    this.logger.debug('Route Found:');
    this.logger.debug(this.routeFound);

    this.clearMapCanvasMarker();

    for (let i = 0; i < this.routeFound.length; i++) {
      this.locationMapper.markLocation(this.markersContext, this.routeFound[i]);
      if (i !== this.routeFound.length - 1) {
        this.markersContext.beginPath();
        this.locationMapper.connectLocation(this.markersContext, this.routeFound[i], this.routeFound[i + 1]);
        this.markersContext.closePath();
        this.markersContext.stroke();
        this.markersContext.save();
      }
    }
  }

  private findRoute(
    destinationLocation: Location,
    currentLocation: Location,
    previousLocation?: Location,
    pendingRoutes: Array<Location> = []): void {

    const currentLocationRouteKey = this.locationMapper.getLocationKeys().find(key =>
      (this.locations[key] as Location).name === currentLocation.name);
    const currentRouteLocations = this.routes[currentLocationRouteKey] as Array<Location>;

    pendingRoutes.push(currentLocation);

    currentRouteLocations.forEach(nextRouteLocation => {
      if (this.routeFound || nextRouteLocation === previousLocation) {
        return;
      }
      this.findRoute(destinationLocation, nextRouteLocation, currentLocation, pendingRoutes);
    });

    if (this.routeFound) {
      return;
    }

    if (pendingRoutes[0] === this.origin.value && pendingRoutes[pendingRoutes.length - 1] === this.destination.value) {
        this.routeFound = [...pendingRoutes];
        return;
    }

    this.possibleRoutes.push([...pendingRoutes]);
    pendingRoutes.pop();
  }
}
