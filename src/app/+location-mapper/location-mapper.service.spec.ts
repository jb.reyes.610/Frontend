import { TestBed, inject } from '@angular/core/testing';

import { LocationMapperService } from './location-mapper.service';

describe('LocationMapperService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LocationMapperService]
    });
  });

  it('should be created', inject([LocationMapperService], (service: LocationMapperService) => {
    expect(service).toBeTruthy();
  }));
});
