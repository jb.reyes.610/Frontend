import { LocationMapperModule } from './location-mapper.module';

describe('LocationMapperModule', () => {
  let locationMapperModule: LocationMapperModule;

  beforeEach(() => {
    locationMapperModule = new LocationMapperModule();
  });

  it('should create an instance', () => {
    expect(locationMapperModule).toBeTruthy();
  });
});
