import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationMapperComponent } from './location-mapper.component';

const routes: Routes = [
  { path: '', component: LocationMapperComponent, pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LocationMapperRoutingModule { }
