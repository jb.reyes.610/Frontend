import { NgModule } from '@angular/core';

import { SharedModule } from '../shared';

import { LocationMapperComponent } from './location-mapper.component';
import { LocationMapperRoutingModule } from './location-mapper-routing.module';
import { LocationMapperService } from './location-mapper.service';

@NgModule({
  imports: [
    SharedModule,
    LocationMapperRoutingModule
  ],
  declarations: [
    LocationMapperComponent
  ],
  providers: [
    LocationMapperService
  ]
})
export class LocationMapperModule { }
