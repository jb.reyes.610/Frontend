import { Injectable } from '@angular/core';

import { contains, getValues, getKeys, sortByName } from '../shared';
import { Location, Locations, Routes, GridMap } from './location-mapper.model';

export const CENTER: GridMap = { lat: 51.678418, lng: 7.809007 };

export const LOCATIONS: Locations = {
  hamm:  { name: 'Hamm', lat: 51.668418, lng: 7.817007, xPos: 642, yPos: 361 },
  munster: { name: 'Münster', lat: 51.96236, lng: 7.62571, xPos: 606.5, yPos: 277.5 },
  dortmund: { name: 'Dortmund', lat: 51.51494, lng: 7.466, xPos: 577, yPos: 409 },
  bielefeld: { name: 'Bielefeld', lat: 52.03333, lng: 8.53333, xPos: 772, yPos: 256 },
  hannover: { name: 'Hannover', lat: 52.37052, lng: 9.73322, xPos: 990, yPos: 154 },
  essen: { name: 'Essen', lat: 51.45657, lng: 7.01228, xPos: 494.5, yPos: 426 },
  duisburg: { name: 'Duisburg', lat: 51.43247, lng: 6.76516, xPos: 449.5, yPos: 432 },
  dusseldorf: { name: 'Düsseldorf', lat: 51.22172, lng: 6.77616, xPos: 451.5, yPos: 493 },
  wuppertal: { name: 'Wuppertal', lat: 51.25027, lng: 7.15055, xPos: 520, yPos: 484 },
  bonn: { name: 'Bonn', lat: 50.73438, lng: 7.098, xPos: 510.5, yPos: 634 },
  cologne: { name: 'Cologne', lat: 50.93333, lng: 6.965, xPos: 485.5, yPos: 577 },
  paderborn: { name: 'Paderborn', lat: 51.71905, lng: 8.75439, xPos: 811.5, yPos: 349 },
  iserlohn: { name: 'Iserlohn', lat: 51.37547, lng: 7.70281, xPos: 619, yPos: 449 },
  duren: { name: 'Düren', lat: 50.80434, lng: 6.49299, xPos: 397, yPos: 616 },
  bocholt: { name: 'Bocholt', lat: 51.83879, lng: 6.61531, xPos: 423, yPos: 314 },
  detmold: { name: 'Detmold', lat: 51.93855, lng: 8.87118, xPos: 832, yPos: 283 },
  lippstadt: { name: 'Lippstadt', lat: 51.67369, lng: 8.34482, xPos: 736, yPos: 361 }
};

export const ROUTES: Routes = {
  hannover: [ LOCATIONS.bielefeld ],
  bielefeld: [ LOCATIONS.hannover, LOCATIONS.paderborn, LOCATIONS.hamm, LOCATIONS.detmold, LOCATIONS.lippstadt ],
  paderborn: [ LOCATIONS.bielefeld ],
  hamm: [ LOCATIONS.bielefeld, LOCATIONS.munster, LOCATIONS.dortmund ],
  munster: [ LOCATIONS.hamm ],
  dortmund: [ LOCATIONS.hamm, LOCATIONS.iserlohn, LOCATIONS.wuppertal ],
  iserlohn: [ LOCATIONS.dortmund ],
  wuppertal: [ LOCATIONS.dortmund, LOCATIONS.essen, LOCATIONS.duisburg, LOCATIONS.dusseldorf, LOCATIONS.cologne ],
  essen: [ LOCATIONS.wuppertal ],
  duisburg: [ LOCATIONS.wuppertal, LOCATIONS.bocholt ],
  dusseldorf: [ LOCATIONS.wuppertal ],
  cologne: [ LOCATIONS.wuppertal, LOCATIONS.duren, LOCATIONS.bonn ],
  duren: [ LOCATIONS.cologne ],
  bonn: [ LOCATIONS.cologne ],
  bocholt: [ LOCATIONS.duisburg ],
  detmold: [ LOCATIONS.bielefeld ],
  lippstadt: [ LOCATIONS.bielefeld ]
};

@Injectable({
  providedIn: 'root'
})
export class LocationMapperService {
  connectLocation(context: CanvasRenderingContext2D, origin: Location, destination: Location): void {
    if (context && origin && destination) {
      context.moveTo(origin.xPos, origin.yPos);
      context.lineTo(destination.xPos, destination.yPos);
    }
  }

  markLocation(context: CanvasRenderingContext2D, location: Location): void {
    if (context && location) {
      context.beginPath();
      context.arc(location.xPos, location.yPos, 3, 0, 2 * Math.PI);
      context.fill();
      context.closePath();
      context.stroke();
      context.save();
    }
  }

  containsLocation(locationList: Array<Location>, location: Location): boolean {
    return contains(locationList, location);
  }

  getLocations(): Locations {
    return LOCATIONS;
  }

  getLocationKeys(): Array<string> {
    return getKeys(LOCATIONS);
  }

  getCenter(): GridMap {
    return CENTER;
  }

  getRoutes(): Routes {
    return ROUTES;
  }

  getRouteKeys(): Array<string> {
    return getKeys(ROUTES);
  }

  getSortedLocations(): Array<Location> {
    return getValues(LOCATIONS).sort(sortByName);
  }
}
