export interface GridMap {
  lat: number;
  lng: number;
}

export interface Location extends GridMap {
  name: string;
  xPos: number;
  yPos: number;
}

export interface Locations {
  [key: string]: Location;
}

export interface Routes {
  [key: string]: Array<Location>;
}
