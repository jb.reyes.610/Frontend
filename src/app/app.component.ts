import { Component, OnInit, ChangeDetectionStrategy, ViewChild } from '@angular/core';

import { TranslateService } from '@ngx-translate/core';

import { RouterStore } from './core';
import { Features, SideNavComponent } from './shared';
import { FEATURES } from './app-routing.module';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent implements OnInit {
  @ViewChild('sideNav') sideNav: SideNavComponent;

  features: Features;

  constructor(
    public routerStore: RouterStore,
    public translate: TranslateService) {

    this.features = FEATURES;
    translate.setDefaultLang('en');
    translate.use('en');
  }

  ngOnInit() { }

  onActivateRoute() {
    if (this.sideNav) {
      this.sideNav.scrollToTop();
    }
  }
}
