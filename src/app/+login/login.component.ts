import { Component, OnInit, ChangeDetectionStrategy, EventEmitter, OnDestroy } from '@angular/core';
import { FormBuilder, FormGroup, FormControl } from '@angular/forms';

import { Subscription } from 'rxjs';

import { AuthStore, LoggerService, RouterStore } from '../core';
import { InputIcon } from '../shared';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit, OnDestroy {
  loginForm: FormGroup;
  username: FormControl;
  password: FormControl;

  hide: EventEmitter<any> = new EventEmitter();
  subscriptions: Subscription = new Subscription;

  suffixIcon: InputIcon = { icon: 'visibility', color: 'primary', click: this.hide };
  passwordType = 'password';
  visible = false;

  constructor(
    public routerStore: RouterStore,
    public formBuilder: FormBuilder,
    public authStore: AuthStore,
    public logger: LoggerService) {

    this.loginForm = formBuilder.group({
      username: 'jbreyes',
      password: 'shit',
    });
    this.username = this.loginForm.get('username') as FormControl;
    this.password = this.loginForm.get('password') as FormControl;
  }

  ngOnInit() {
    this.subscriptions.add(this.hide.subscribe(() => this.onClickVisibility()));
    this.subscriptions.add(this.authStore.error$.subscribe(error => {
      if (error) {
        this.logger.error(error);
      }
    }));
    this.subscriptions.add(this.authStore.loading$.subscribe(loading =>
      loading ? this.loginForm.disable() : this.loginForm.enable()));

    this.subscriptions.add(this.routerStore.data$.subscribe(data => this.logger.debug(data)));
  }

  ngOnDestroy() {
    this.subscriptions.unsubscribe();
  }

  onClickVisibility(): void {
    this.visible = !this.visible;
    if (this.visible) {
      this.suffixIcon = { ...this.suffixIcon, icon: 'visibility_off' };
      this.passwordType = 'text';
    } else {
      this.suffixIcon = { ...this.suffixIcon, icon: 'visibility' };
      this.passwordType = 'password';
    }
  }

  onSubmit(): void {
    if (this.loginForm.valid) {
      this.authStore.login({ username: this.username.value, password: this.password.value });
    }
  }
}
