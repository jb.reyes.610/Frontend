export const sortByName = (compare1: any, compare2: any) => {
  if (compare1 && compare2) {
    if (compare1.name < compare2.name) {
      return -1;
    }
    if (compare1.name > compare2.name) {
      return 1;
    }
  }
  return 0;
};
