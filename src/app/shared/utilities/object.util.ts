export const getKeys = (obj: any): Array<any> => {
  return Object.keys(obj);
};

export const getValues = (obj: any): Array<any> => {
  return Object.values(obj);
};

export const findKey = (obj: any, value: string): any => {
  return getKeys(obj).find(key => obj[key] === value);
};

export const contains = (arr: Array<any>, value: any): boolean => {
  return arr.indexOf(value) !== -1;
};
