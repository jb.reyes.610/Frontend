const typeCache: { [label: string]: boolean } = {};

export const type = <T>(label: T | ''): T => {
  if (typeCache[<string>label]) {
    throw new Error(`Action type '${label}' is already used.`);
  }
  typeCache[<string>label] = true;
  return <T>label;
};
