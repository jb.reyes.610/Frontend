import { Error } from './error.model';

export interface Response {
  status: boolean;

  error?: Error;
  data?: any;
  message?: string;
}
