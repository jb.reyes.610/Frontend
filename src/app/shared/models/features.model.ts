export interface Feature {
  name: string;
  path: string;

  background?: string;
  preload?: boolean;
  state?: string;
  title?: string;
}

export interface Features {
  [key: string]: Feature;
}
