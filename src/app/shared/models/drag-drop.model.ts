export interface DraggableOptions {
  zone?: string;
  data?: any;
}

export interface DroppableOptions {
  zone?: string;
}
