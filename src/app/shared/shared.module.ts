import { NgModule, ModuleWithProviders } from '@angular/core';

import { AgmCoreModule } from '@agm/core';
import { TranslateModule } from '@ngx-translate/core';

import { AngularModule, MaterialModule } from './modules';
import { ComponentsModule } from './components';
import { DraggableDirective, DroppableDirective } from './directives';

@NgModule({
  declarations: [
    DraggableDirective,
    DroppableDirective
  ],
  exports: [
    AngularModule,
    MaterialModule,
    ComponentsModule,
    AgmCoreModule,
    TranslateModule,
    DraggableDirective,
    DroppableDirective
  ],

})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule
    };
  }
}
