import { ThemePalette } from '@angular/material';
import { EventEmitter } from '@angular/core';

export interface InputIcon {
  icon: string;

  inline?: boolean;
  class?: string;
  click?: EventEmitter<any>;
  color?: ThemePalette;
}
