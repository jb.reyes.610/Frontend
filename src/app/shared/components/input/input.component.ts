import { Component, OnInit, ChangeDetectionStrategy, Input, Optional } from '@angular/core';
import { AbstractControl, FormControl } from '@angular/forms';

import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';

import { getKeys, contains } from '../../utilities';
import { FormFieldComponent } from '../form-field';
import { InputIcon } from './input.model';

@Component({
  selector: 'app-input',
  templateUrl: './input.component.html',
  styleUrls: ['./input.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InputComponent extends FormFieldComponent implements OnInit {
  @Input() required: boolean;

  @Input() hint: string;
  @Input() i18nMessageBase: string;
  @Input() label: string;
  @Input() placeholder: string;
  @Input() type: string;

  @Input() autocompleteOptions: Array<any> = [];
  @Input() autocompleteValueField: string;

  @Input() control: AbstractControl = new FormControl();

  @Input() prefixIcon: InputIcon;
  @Input() suffixIcon: InputIcon;

  filteredOptions: Observable<any[]>;

  autocompleteDisplay = (option?: any): string | undefined => {
    if (this.autocompleteValueField) {
      return option ? option[this.autocompleteValueField] : undefined;
    }
    return option;
  }

  constructor() {
    super();
  }

  ngOnInit() {
    if (this.control) {
      this.filteredOptions = this.control.valueChanges.pipe(
        startWith<string | any>(''),
        map(value => typeof value === 'string' ? value : value[this.autocompleteValueField]),
        map(value => value ? this.autocompleteFilter(value) : this.autocompleteOptions.slice())
      );
    }
  }

  getErrors(errorObj: any) {
    return errorObj ? getKeys(errorObj) : [];
  }

  private autocompleteFilter(value: string): any[] {
    const filterValue = value.toLowerCase();

    return this.autocompleteOptions.filter(option => this.autocompleteValueField ?
      contains(option[this.autocompleteValueField].toLowerCase(), filterValue) :
      contains(option.toLowerCase(), filterValue));
  }
}
