import { EventEmitter } from '@angular/core';
import { ThemePalette } from '@angular/material';

export type ButtonBehavior = 'submit' | 'reset' | 'button';
export type ButtonType = 'button' | 'raised' | 'icon' | 'fab' | 'mini-fab' | 'stroked' | 'flat';

export interface Button {
  disabled?: boolean;
  icon?: string;
  label?: string;
  type?: ButtonType;
  color?: ThemePalette;
  behavior?: ButtonBehavior;

  cmClick?: EventEmitter<any>;
}
