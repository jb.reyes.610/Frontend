import { Component, OnInit, ChangeDetectionStrategy, Input, Output, EventEmitter } from '@angular/core';
import { ThemePalette } from '@angular/material';

import { ButtonBehavior, ButtonType } from './button.model';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ButtonComponent implements OnInit {
  @Input() disabled: boolean;
  @Input() icon: string;
  @Input() label: string;
  @Input() type: ButtonType;
  @Input() color: ThemePalette;
  @Input() behavior: ButtonBehavior = 'submit';

  @Output() cmClick: EventEmitter<any> = new EventEmitter();

  constructor() { }

  ngOnInit() {
  }

  onClick(event: any): void {
    this.cmClick.emit(event);
  }
}
