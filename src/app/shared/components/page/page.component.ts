import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';

import { Button } from '../button';

@Component({
  selector: 'app-page',
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageComponent {
  @Input() title: string;
  @Input() buttons: Array<Button>;
}
