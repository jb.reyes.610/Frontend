import {
  Component, ChangeDetectionStrategy, ChangeDetectorRef, ElementRef, Input, OnInit, ViewChild
} from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSidenav } from '@angular/material';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { getKeys } from '../../utilities';
import { Features } from '../../models';

@Component({
  selector: 'app-side-nav',
  templateUrl: './side-nav.component.html',
  styleUrls: ['./side-nav.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SideNavComponent implements OnInit {
  @ViewChild('sideNav') sideNav: MatSidenav;
  @ViewChild('content', { read: ElementRef }) content: ElementRef;

  @Input() contentClass: string;
  @Input() features: Features;

  featureKeys: Array<string>;

  isBreakpoint$: Observable<boolean> = this.breakpointObserver.observe('(max-width: 600px)').pipe(
    map(result => result.matches)
  );

  constructor(
    private breakpointObserver: BreakpointObserver,
    private changeDetector: ChangeDetectorRef) {

  }

  ngOnInit() {
    this.featureKeys = getKeys(this.features);
  }

  toggleSideNav(): void {
    this.sideNav.toggle();
    this.changeDetector.markForCheck();
  }

  onClickNav(event: any): void {
    if (this.sideNav.mode === 'over' && this.sideNav.opened) {
      this.sideNav.close();
    }
  }

  scrollToTop(): void {
    this.content.nativeElement.scroll({ left: 0, top: 0, behavior: 'smooth'});
  }
}
