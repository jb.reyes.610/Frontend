import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { TranslateModule } from '@ngx-translate/core';

import { AngularModule, MaterialModule } from '../modules';

import { ErrorPageComponent } from './error-page';
import { FooterComponent } from './footer';
import { SideNavComponent } from './side-nav';
import { ToolbarComponent } from './toolbar';
import { InputComponent } from './input';
import { FormFieldComponent } from './form-field';
import { PageComponent } from './page';
import { ButtonComponent } from './button';

@NgModule({
  imports: [
    AngularModule,
    MaterialModule,
    RouterModule,
    TranslateModule
  ],
  declarations: [
    ErrorPageComponent,
    FooterComponent,
    SideNavComponent,
    ToolbarComponent,
    InputComponent,
    FormFieldComponent,
    PageComponent,
    ButtonComponent
  ],
  exports: [
    ErrorPageComponent,
    FooterComponent,
    SideNavComponent,
    ToolbarComponent,
    InputComponent,
    PageComponent,
    ButtonComponent
  ]
})
export class ComponentsModule { }
