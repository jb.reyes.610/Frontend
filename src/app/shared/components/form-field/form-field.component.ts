import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { MatFormFieldAppearance, FloatLabelType } from '@angular/material';

@Component({
  selector: 'app-form-field',
  templateUrl: './form-field.component.html',
  styleUrls: ['./form-field.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FormFieldComponent {
  @Input() class: string;
  @Input() appearance: MatFormFieldAppearance;
  @Input() hideRequired: boolean;
  @Input() floatLabel: FloatLabelType;
  @Input() hintLabel: string;
}
