import { Directive, HostBinding, HostListener, Input } from '@angular/core';

import { DraggableOptions } from '../models';
import { DragDropService } from '../services';

@Directive({
  selector: '[appDraggable]'
})
export class DraggableDirective {
  constructor(private dragService?: DragDropService) { }

  @HostBinding('draggable')
  get draggable() {
    return true;
  }

  @Input()
  set appDraggable(options: DraggableOptions) {
    if (options) {
      this.options = options;
    }
  }

  private options: DraggableOptions = {};

  @HostListener('dragstart', ['$event'])
  onDragStart(event) {
    const { zone = 'zone', data = {} } = this.options;
    this.dragService.startDrag(zone);
    event.dataTransfer.setData('data', JSON.stringify({ ...data, xPosOffsetSrc: event.offsetX, yPosOffsetSrc: event.offsetY }));
  }
}
