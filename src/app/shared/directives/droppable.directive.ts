import { Directive, EventEmitter, HostListener, Input, Output } from '@angular/core';

import { DroppableOptions } from '../models';
import { DragDropService } from '../services';

@Directive({
  selector: '[appDroppable]'
})
export class DroppableDirective {
  constructor(private dragService?: DragDropService) { }

  @Input()
  set appDroppable(options: DroppableOptions) {
    if (options) {
      this.options = options;
    }
  }

  @Output() appDrop = new EventEmitter();

  options: DroppableOptions = {};

  @HostListener('dragenter', ['$event'])
  @HostListener('dragover', ['$event'])
  onDragOver(event) {
    const { zone = 'zone' } = this.options;
    if (this.dragService.accepts(zone)) {
       event.preventDefault();
    }
  }

  @HostListener('drop', ['$event'])
  onDrop(event) {
    const data =  JSON.parse(event.dataTransfer.getData('data'));
    this.appDrop.next({...data, xPosOffsetTgt: event.offsetX, yPosOffsetTgt: event.offsetY });
  }
}
